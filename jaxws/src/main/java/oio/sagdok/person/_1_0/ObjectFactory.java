
package oio.sagdok.person._1_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import oio.sagdok._2_0.SoegOutputType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the oio.sagdok.person._1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SoegInput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "SoegInput");
    private final static QName _ListOejebliksbilledeInput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "ListOejebliksbilledeInput");
    private final static QName _LaesOutput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "LaesOutput");
    private final static QName _LaesPeriodInput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "LaesPeriodInput");
    private final static QName _GetUuidOutput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "GetUuidOutput");
    private final static QName _ListOutput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "ListOutput");
    private final static QName _LaesOejebliksbilledeInput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "LaesOejebliksbilledeInput");
    private final static QName _SoegOutput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "SoegOutput");
    private final static QName _ListPeriodInput_QNAME = new QName("urn:oio:sagdok:person:1.0.0", "ListPeriodInput");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: oio.sagdok.person._1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOutputType }
     * 
     */
    public ListOutputType createListOutputType() {
        return new ListOutputType();
    }

    /**
     * Create an instance of {@link LaesPeriodInputType }
     * 
     */
    public LaesPeriodInputType createLaesPeriodInputType() {
        return new LaesPeriodInputType();
    }

    /**
     * Create an instance of {@link ListOejebliksbilledeInputType }
     * 
     */
    public ListOejebliksbilledeInputType createListOejebliksbilledeInputType() {
        return new ListOejebliksbilledeInputType();
    }

    /**
     * Create an instance of {@link GetUuidOutputType }
     * 
     */
    public GetUuidOutputType createGetUuidOutputType() {
        return new GetUuidOutputType();
    }

    /**
     * Create an instance of {@link ListPeriodInputType }
     * 
     */
    public ListPeriodInputType createListPeriodInputType() {
        return new ListPeriodInputType();
    }

    /**
     * Create an instance of {@link LaesOejebliksbilledeInputType }
     * 
     */
    public LaesOejebliksbilledeInputType createLaesOejebliksbilledeInputType() {
        return new LaesOejebliksbilledeInputType();
    }

    /**
     * Create an instance of {@link SoegInputType }
     * 
     */
    public SoegInputType createSoegInputType() {
        return new SoegInputType();
    }

    /**
     * Create an instance of {@link LaesOutputType }
     * 
     */
    public LaesOutputType createLaesOutputType() {
        return new LaesOutputType();
    }

    /**
     * Create an instance of {@link KontaktKanalBaseType }
     * 
     */
    public KontaktKanalBaseType createKontaktKanalBaseType() {
        return new KontaktKanalBaseType();
    }

    /**
     * Create an instance of {@link RelationListeType }
     * 
     */
    public RelationListeType createRelationListeType() {
        return new RelationListeType();
    }

    /**
     * Create an instance of {@link SoegRelationListeType }
     * 
     */
    public SoegRelationListeType createSoegRelationListeType() {
        return new SoegRelationListeType();
    }

    /**
     * Create an instance of {@link SoegEgenskabType }
     * 
     */
    public SoegEgenskabType createSoegEgenskabType() {
        return new SoegEgenskabType();
    }

    /**
     * Create an instance of {@link RegistreringType }
     * 
     */
    public RegistreringType createRegistreringType() {
        return new RegistreringType();
    }

    /**
     * Create an instance of {@link AndenKontaktKanalType }
     * 
     */
    public AndenKontaktKanalType createAndenKontaktKanalType() {
        return new AndenKontaktKanalType();
    }

    /**
     * Create an instance of {@link GetUuidArrayOutputType }
     * 
     */
    public GetUuidArrayOutputType createGetUuidArrayOutputType() {
        return new GetUuidArrayOutputType();
    }

    /**
     * Create an instance of {@link DanskAdresseType }
     * 
     */
    public DanskAdresseType createDanskAdresseType() {
        return new DanskAdresseType();
    }

    /**
     * Create an instance of {@link AdresseType }
     * 
     */
    public AdresseType createAdresseType() {
        return new AdresseType();
    }

    /**
     * Create an instance of {@link SoegTilstandListeType }
     * 
     */
    public SoegTilstandListeType createSoegTilstandListeType() {
        return new SoegTilstandListeType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link SoegAttributListeType }
     * 
     */
    public SoegAttributListeType createSoegAttributListeType() {
        return new SoegAttributListeType();
    }

    /**
     * Create an instance of {@link PeriodInputType }
     * 
     */
    public PeriodInputType createPeriodInputType() {
        return new PeriodInputType();
    }

    /**
     * Create an instance of {@link AdresseBaseType }
     * 
     */
    public AdresseBaseType createAdresseBaseType() {
        return new AdresseBaseType();
    }

    /**
     * Create an instance of {@link VerdenAdresseType }
     * 
     */
    public VerdenAdresseType createVerdenAdresseType() {
        return new VerdenAdresseType();
    }

    /**
     * Create an instance of {@link OejebliksbilledeInputType }
     * 
     */
    public OejebliksbilledeInputType createOejebliksbilledeInputType() {
        return new OejebliksbilledeInputType();
    }

    /**
     * Create an instance of {@link PersonRelationType }
     * 
     */
    public PersonRelationType createPersonRelationType() {
        return new PersonRelationType();
    }

    /**
     * Create an instance of {@link TelefonType }
     * 
     */
    public TelefonType createTelefonType() {
        return new TelefonType();
    }

    /**
     * Create an instance of {@link LivStatusType }
     * 
     */
    public LivStatusType createLivStatusType() {
        return new LivStatusType();
    }

    /**
     * Create an instance of {@link OpretOutputType }
     * 
     */
    public OpretOutputType createOpretOutputType() {
        return new OpretOutputType();
    }

    /**
     * Create an instance of {@link RegisterOplysningType }
     * 
     */
    public RegisterOplysningType createRegisterOplysningType() {
        return new RegisterOplysningType();
    }

    /**
     * Create an instance of {@link SoegObjektType }
     * 
     */
    public SoegObjektType createSoegObjektType() {
        return new SoegObjektType();
    }

    /**
     * Create an instance of {@link FiltreretOejebliksbilledeType }
     * 
     */
    public FiltreretOejebliksbilledeType createFiltreretOejebliksbilledeType() {
        return new FiltreretOejebliksbilledeType();
    }

    /**
     * Create an instance of {@link SundhedOplysningType }
     * 
     */
    public SundhedOplysningType createSundhedOplysningType() {
        return new SundhedOplysningType();
    }

    /**
     * Create an instance of {@link AttributListeType }
     * 
     */
    public AttributListeType createAttributListeType() {
        return new AttributListeType();
    }

    /**
     * Create an instance of {@link RegisterOplysningBaseType }
     * 
     */
    public RegisterOplysningBaseType createRegisterOplysningBaseType() {
        return new RegisterOplysningBaseType();
    }

    /**
     * Create an instance of {@link UkendtBorgerType }
     * 
     */
    public UkendtBorgerType createUkendtBorgerType() {
        return new UkendtBorgerType();
    }

    /**
     * Create an instance of {@link LaesResultatType }
     * 
     */
    public LaesResultatType createLaesResultatType() {
        return new LaesResultatType();
    }

    /**
     * Create an instance of {@link EgenskabType }
     * 
     */
    public EgenskabType createEgenskabType() {
        return new EgenskabType();
    }

    /**
     * Create an instance of {@link TilstandListeType }
     * 
     */
    public TilstandListeType createTilstandListeType() {
        return new TilstandListeType();
    }

    /**
     * Create an instance of {@link KontaktKanalType }
     * 
     */
    public KontaktKanalType createKontaktKanalType() {
        return new KontaktKanalType();
    }

    /**
     * Create an instance of {@link CivilStatusType }
     * 
     */
    public CivilStatusType createCivilStatusType() {
        return new CivilStatusType();
    }

    /**
     * Create an instance of {@link GroenlandAdresseType }
     * 
     */
    public GroenlandAdresseType createGroenlandAdresseType() {
        return new GroenlandAdresseType();
    }

    /**
     * Create an instance of {@link CprBorgerType }
     * 
     */
    public CprBorgerType createCprBorgerType() {
        return new CprBorgerType();
    }

    /**
     * Create an instance of {@link NavnStrukturType }
     * 
     */
    public NavnStrukturType createNavnStrukturType() {
        return new NavnStrukturType();
    }

    /**
     * Create an instance of {@link UdenlandskBorgerType }
     * 
     */
    public UdenlandskBorgerType createUdenlandskBorgerType() {
        return new UdenlandskBorgerType();
    }

    /**
     * Create an instance of {@link RegistreringOutputType }
     * 
     */
    public RegistreringOutputType createRegistreringOutputType() {
        return new RegistreringOutputType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SoegInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "SoegInput")
    public JAXBElement<SoegInputType> createSoegInput(SoegInputType value) {
        return new JAXBElement<SoegInputType>(_SoegInput_QNAME, SoegInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOejebliksbilledeInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "ListOejebliksbilledeInput")
    public JAXBElement<ListOejebliksbilledeInputType> createListOejebliksbilledeInput(ListOejebliksbilledeInputType value) {
        return new JAXBElement<ListOejebliksbilledeInputType>(_ListOejebliksbilledeInput_QNAME, ListOejebliksbilledeInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaesOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "LaesOutput")
    public JAXBElement<LaesOutputType> createLaesOutput(LaesOutputType value) {
        return new JAXBElement<LaesOutputType>(_LaesOutput_QNAME, LaesOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaesPeriodInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "LaesPeriodInput")
    public JAXBElement<LaesPeriodInputType> createLaesPeriodInput(LaesPeriodInputType value) {
        return new JAXBElement<LaesPeriodInputType>(_LaesPeriodInput_QNAME, LaesPeriodInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUuidOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "GetUuidOutput")
    public JAXBElement<GetUuidOutputType> createGetUuidOutput(GetUuidOutputType value) {
        return new JAXBElement<GetUuidOutputType>(_GetUuidOutput_QNAME, GetUuidOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "ListOutput")
    public JAXBElement<ListOutputType> createListOutput(ListOutputType value) {
        return new JAXBElement<ListOutputType>(_ListOutput_QNAME, ListOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaesOejebliksbilledeInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "LaesOejebliksbilledeInput")
    public JAXBElement<LaesOejebliksbilledeInputType> createLaesOejebliksbilledeInput(LaesOejebliksbilledeInputType value) {
        return new JAXBElement<LaesOejebliksbilledeInputType>(_LaesOejebliksbilledeInput_QNAME, LaesOejebliksbilledeInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SoegOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "SoegOutput")
    public JAXBElement<SoegOutputType> createSoegOutput(SoegOutputType value) {
        return new JAXBElement<SoegOutputType>(_SoegOutput_QNAME, SoegOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPeriodInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:oio:sagdok:person:1.0.0", name = "ListPeriodInput")
    public JAXBElement<ListPeriodInputType> createListPeriodInput(ListPeriodInputType value) {
        return new JAXBElement<ListPeriodInputType>(_ListPeriodInput_QNAME, ListPeriodInputType.class, null, value);
    }

}
