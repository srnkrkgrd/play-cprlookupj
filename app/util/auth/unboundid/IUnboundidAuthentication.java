package util.auth.unboundid;

import util.auth.IAuthentication;

/**
 * Extention of the IAuthentication.
 * @author Søren Kirkegård
 *
 */
public interface IUnboundidAuthentication extends IAuthentication {

}
